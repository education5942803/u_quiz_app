import 'package:flutter/material.dart';
import 'package:u_quiz_app/models/model_question.dart';

import 'data/data_questions.dart';
import 'question.dart';

class QuestionsScreen extends StatefulWidget {
  const QuestionsScreen({super.key, required this.onSelectAnswer});

  final void Function(String answer) onSelectAnswer;

  @override
  State<QuestionsScreen> createState() => _QuestionsScreenState();
}

class _QuestionsScreenState extends State<QuestionsScreen> {
  int activeQuestionIndex = 0;
  final int questionsLength = dataQuestions.length;

  void onAnswer(String answer) {
    widget.onSelectAnswer(answer);
    setState(() => activeQuestionIndex++);
  }

  @override
  Widget build(BuildContext context) {
    final ModelQuestion currQuestion = dataQuestions[activeQuestionIndex];

    return Container(
      padding: const EdgeInsets.all(32.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Question(
            currQuestion.question,
            currQuestion.shuffledAnswers,
            onAnswer: onAnswer,
          ),
        ],
      ),
    );
  }
}
