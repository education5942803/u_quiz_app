import 'package:flutter/material.dart';

class SummaryItemContent extends StatelessWidget {
  const SummaryItemContent({
    super.key,
    required this.question,
    required this.answerCorrect,
    required this.answerSelected,
    this.indicationColors = const {
      'color_correct': Color.fromRGBO(0, 234, 255, 0.85),
      'color_error': Color.fromRGBO(213, 86, 201, 0.85),
    },
  });

  final String question;
  final String answerCorrect;
  final String answerSelected;
  final Map<String, Color> indicationColors;

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            question,
            style: const TextStyle(
              color: Colors.white,
              fontSize: 18.0,
              fontWeight: FontWeight.w700,
            ),
          ),
          const SizedBox(height: 12.0),
          Text(
            answerCorrect,
            style: TextStyle(color: indicationColors['color_correct']),
          ),
          const SizedBox(height: 6.0),
          Text(
            answerSelected,
            style: TextStyle(color: indicationColors['color_error']),
          ),
        ],
      ),
    );
  }
}
