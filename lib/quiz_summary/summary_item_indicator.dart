import 'package:flutter/material.dart';

class SummaryItemIndicator extends StatelessWidget {
  const SummaryItemIndicator(
    this.indicatorText, {
    super.key,
    required this.isAnswerCorrect,
    this.indicationColors = const {
      'color_correct': Color.fromRGBO(0, 234, 255, 0.85),
      'color_error': Color.fromRGBO(213, 86, 201, 0.85),
    },
  });

  final String indicatorText;
  final bool isAnswerCorrect;
  final Map<String, Color> indicationColors;

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.center,
      width: 36.0,
      height: 36.0,
      decoration: BoxDecoration(
        color: isAnswerCorrect
            ? indicationColors['color_correct']
            : indicationColors['color_error'],
        borderRadius: BorderRadius.circular(50.0),
      ),
      child: Text(
        indicatorText,
        style: const TextStyle(
          color: Colors.black,
          fontSize: 18.0,
          fontWeight: FontWeight.w700,
        ),
        textAlign: TextAlign.start,
      ),
    );
  }
}
