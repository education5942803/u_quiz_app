import 'package:flutter/material.dart';

import 'summary_item_content.dart';
import 'summary_item_indicator.dart';

class QuizSummary extends StatelessWidget {
  const QuizSummary(this.data, {super.key});

  final List<Map<String, Object>> data;

  @override
  Widget build(BuildContext context) {
    const Map<String, Color> indicationColors = {
      'color_correct': Color.fromRGBO(0, 234, 255, 0.85),
      'color_error': Color.fromRGBO(213, 86, 201, 0.85),
    };

    return SizedBox(
      height: 400.0,
      child: SingleChildScrollView(
        child: Column(
          children: data.map(
            (item) {
              final qIndicatorText =
                  ((item['question_index'] as int) + 1).toString();
              final qText = item['question_text'] as String;
              final qAnswerCorrect = item['answer_correct'] as String;
              final qAnswerSelected = item['answer_selected'] as String;
              final bool isAnswerCorrect = qAnswerCorrect == qAnswerSelected;

              return Container(
                margin: const EdgeInsets.symmetric(vertical: 16.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    SummaryItemIndicator(
                      qIndicatorText,
                      isAnswerCorrect: isAnswerCorrect,
                      indicationColors: indicationColors,
                    ),
                    const SizedBox(width: 20.0),
                    SummaryItemContent(
                      question: qText,
                      answerCorrect: qAnswerCorrect,
                      answerSelected: qAnswerSelected,
                      indicationColors: indicationColors,
                    ),
                  ],
                ),
              );
            },
          ).toList(),
        ),
      ),
    );
  }
}
