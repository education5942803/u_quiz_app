import 'package:flutter/material.dart';

const double CNST_GAP_BETWEEN = 28.0;
const Color CNST_COL_TRANS_075 = Color.fromRGBO(255, 255, 255, 0.75);

class QuizStartScreen extends StatelessWidget {
  const QuizStartScreen({super.key, required this.onBtnPressed});

  final void Function() onBtnPressed;

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Image.asset(
          'assets/images/logo.png',
          width: 280.0,
          color: CNST_COL_TRANS_075,
        ),
        const SizedBox(height: CNST_GAP_BETWEEN),
        const Text(
          'Learn Flutter the fun way!',
          style: TextStyle(
            color: Colors.white,
            fontSize: 24.0,
          ),
        ),
        const SizedBox(height: CNST_GAP_BETWEEN),
        OutlinedButton.icon(
          style: OutlinedButton.styleFrom(
            foregroundColor: Colors.white,
          ),
          onPressed: onBtnPressed,
          icon: const Icon(Icons.arrow_right_alt),
          label: const Text('Start Quiz'),
        ),
      ],
    );
  }
}
