import 'package:flutter/material.dart';
import 'package:u_quiz_app/quiz_start_screen.dart';

import 'data/data_questions.dart';
import 'questions_screen.dart';
import 'results_screen.dart';

const Alignment beginGradAlignment = Alignment.topLeft;
const Alignment endGradAlignment = Alignment.bottomRight;

const List<Color> defGradColors = <Color>[
  Color.fromARGB(255, 71, 16, 168),
  Color.fromARGB(255, 85, 16, 168),
  Color.fromARGB(255, 101, 16, 168),
];

class UQuizApp extends StatefulWidget {
  const UQuizApp({super.key});

  @override
  State<UQuizApp> createState() => _UQuizAppState();
}

class _UQuizAppState extends State<UQuizApp> {
  List<String> selectedAnswers = <String>[];
  String activeScreenId = 'start-screen';

  void switchScreen(String screenId) {
    setState(() => activeScreenId = screenId);
  }

  void selectAnswer(String answer) {
    selectedAnswers.add(answer);

    if (selectedAnswers.length == dataQuestions.length) {
      setState(() {
        activeScreenId = 'results-screen';
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    Widget? activeScreen;

    switch (activeScreenId) {
      case 'questions-screen':
        activeScreen = QuestionsScreen(onSelectAnswer: selectAnswer);
      case 'results-screen':
        activeScreen = ResultsScreen(
          answers: selectedAnswers,
          onRestartQuiz: () => switchScreen('questions-screen'),
        );
        selectedAnswers = <String>[];
      default:
        activeScreen = QuizStartScreen(
          onBtnPressed: () => switchScreen('questions-screen'),
        );
        selectedAnswers = <String>[];
    }

    return MaterialApp(
      home: Scaffold(
        body: Container(
          decoration: const BoxDecoration(
            gradient: LinearGradient(
              colors: defGradColors,
              begin: beginGradAlignment,
              end: endGradAlignment,
            ),
          ),
          child: SizedBox(
            width: double.infinity,
            child: activeScreen,
          ),
        ),
      ),
    );
  }
}
