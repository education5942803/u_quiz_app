import 'package:flutter/material.dart';

import 'data/data_questions.dart';
import 'quiz_summary/quiz_summary.dart';

class ResultsScreen extends StatelessWidget {
  const ResultsScreen({
    super.key,
    required this.answers,
    required this.onRestartQuiz,
  });

  final List<String> answers;
  final void Function() onRestartQuiz;

  List<Map<String, Object>> get summaryData {
    final List<Map<String, Object>> summary = [];

    for (int i = 0; i < answers.length; i++) {
      summary.add(
        {
          'question_index': i,
          'question_text': dataQuestions[i].question,
          'answer_correct': dataQuestions[i].answers[0],
          'answer_selected': answers[i],
        },
      );
    }

    return summary;
  }

  @override
  Widget build(BuildContext context) {
    final int totalQuestionsCount = dataQuestions.length;
    final int correctAnswersCount = summaryData.where((item) {
      return item['answer_correct'] == item['answer_selected'];
    }).length;

    return SizedBox(
      width: double.infinity,
      child: Container(
        margin: const EdgeInsets.all(40.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              'You answered $correctAnswersCount out of $totalQuestionsCount questions correctly!',
              style: const TextStyle(
                color: Color.fromRGBO(255, 255, 255, 0.75),
                fontSize: 20.0,
              ),
              textAlign: TextAlign.center,
            ),
            const SizedBox(height: 20.0),
            QuizSummary(summaryData),
            const SizedBox(height: 20.0),
            TextButton.icon(
              style: TextButton.styleFrom(
                foregroundColor: Colors.white,
              ),
              onPressed: onRestartQuiz,
              icon: const Icon(Icons.refresh),
              label: const Text(
                'Restart quiz?',
                style: TextStyle(fontSize: 20.0),
                textAlign: TextAlign.center,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
