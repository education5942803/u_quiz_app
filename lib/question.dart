import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class Question extends StatelessWidget {
  const Question(
    this.question,
    this.answers, {
    super.key,
    required this.onAnswer,
  });

  final String question;
  final List<String> answers;
  final void Function(String answer) onAnswer;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(vertical: 28.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Text(
            question,
            style: GoogleFonts.courierPrime(
              color: Colors.white,
              fontSize: 28.0,
              fontWeight: FontWeight.w700,
            ),
            textAlign: TextAlign.center,
          ),
          const SizedBox(height: 18.0),
          ...answers.map((String answer) {
            return ElevatedButton(
                onPressed: () => onAnswer(answer),
                child: Text(
                  answer,
                  textAlign: TextAlign.center,
                ));
          })
        ],
      ),
    );
  }
}
