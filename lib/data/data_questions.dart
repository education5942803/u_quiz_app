import '../models/model_question.dart';

const List<ModelQuestion> dataQuestions = <ModelQuestion>[
  ModelQuestion(
    'Question #1',
    <String>['Q1-A1', 'Q1-A2', 'Q1-A3'],
  ),
  ModelQuestion(
    'Question #2',
    <String>['Q2-A1', 'Q2-A2', 'Q2-A3'],
  ),
  ModelQuestion(
    'Question #3',
    <String>['Q3-A1', 'Q3-A2', 'Q3-A3'],
  ),
  ModelQuestion(
    'Question #4',
    <String>['Q4-A1', 'Q4-A2', 'Q4-A3'],
  ),
  ModelQuestion(
    'Question #5',
    <String>['Q5-A1', 'Q5-A2', 'Q5-A3'],
  ),
  ModelQuestion(
    'Question #6',
    <String>['Q6-A1', 'Q6-A2', 'Q6-A3'],
  ),
  ModelQuestion(
    'Question #7',
    <String>['Q7-A1', 'Q7-A2', 'Q7-A3'],
  ),
];
